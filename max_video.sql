/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : max_video

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 25/12/2022 13:43:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admintor
-- ----------------------------
DROP TABLE IF EXISTS `admintor`;
CREATE TABLE `admintor`  (
  `id` int NOT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职位',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admintor
-- ----------------------------
INSERT INTO `admintor` VALUES (1, '123456', '超级管理员');
INSERT INTO `admintor` VALUES (2, '123456', '网站管理员');

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history`  (
  `user_id` int NULL DEFAULT NULL,
  `video_id` int NULL DEFAULT NULL,
  `times` int NULL DEFAULT NULL,
  INDEX `history_user`(`user_id` ASC) USING BTREE,
  INDEX `history_video`(`video_id` ASC) USING BTREE,
  CONSTRAINT `history_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `history_video` FOREIGN KEY (`video_id`) REFERENCES `movie` (`video_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of history
-- ----------------------------
INSERT INTO `history` VALUES (1, 2, 30);
INSERT INTO `history` VALUES (1, 6, 1);
INSERT INTO `history` VALUES (1, 1, 7);
INSERT INTO `history` VALUES (2, 2, 3);
INSERT INTO `history` VALUES (2, 8, 2);
INSERT INTO `history` VALUES (1, 7, 2);
INSERT INTO `history` VALUES (1, 8, 4);
INSERT INTO `history` VALUES (4, 2, 5);
INSERT INTO `history` VALUES (4, 10, 1);
INSERT INTO `history` VALUES (4, 1, 1);
INSERT INTO `history` VALUES (4, 5, 1);
INSERT INTO `history` VALUES (1, 10, 1);
INSERT INTO `history` VALUES (7, 2, 2);
INSERT INTO `history` VALUES (7, 1, 2);
INSERT INTO `history` VALUES (2, 10, 4);
INSERT INTO `history` VALUES (2, 1, 2);

-- ----------------------------
-- Table structure for movie
-- ----------------------------
DROP TABLE IF EXISTS `movie`;
CREATE TABLE `movie`  (
  `video_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` int NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`video_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of movie
-- ----------------------------
INSERT INTO `movie` VALUES (1, '【玩偶】下次见面还会记得我眼里的光吗？', 'video/video1.mp4', 342221, '美女');
INSERT INTO `movie` VALUES (2, '还不赶紧艾特你的好兄弟来看？', 'video/video2.mp4', 26291, '美女');
INSERT INTO `movie` VALUES (3, '【奶糖】白发萝莉纸尿裤 我叫派蒙你记住！', 'video/video3.mp4', 89448, '美女');
INSERT INTO `movie` VALUES (4, '童年回忆 GGBOY 小呆呆为何疯狂射击？', 'video/video4.mp4', 1326, '动漫');
INSERT INTO `movie` VALUES (5, '【韩流】起床啦 你的兄弟喊你起来看美女热舞了 ', 'video/video5.mp4', 19999, '美女');
INSERT INTO `movie` VALUES (6, '奔跑吧琴酒 百褶裙 舞蹈合集 ', 'video/video6.mp4', 2198, ' 美女');
INSERT INTO `movie` VALUES (7, '你有见过这么小的奥特曼吗？迷你奥特曼 ', 'video/video7.mp4', 3456, '动漫');
INSERT INTO `movie` VALUES (8, '【JK】这样的美腿吸引到你了吗？', 'video/video8.mp4', 222371, '美女');
INSERT INTO `movie` VALUES (9, '【韵白影视】丈夫触电倒地，老婆这样做...', 'video/video9.mp4', 7865, '影视');
INSERT INTO `movie` VALUES (10, '【华为】mate50pro 真旗舰 不畏摔 ', 'video/video10.mp4', 6749, '剪辑');
INSERT INTO `movie` VALUES (11, '这样的画面你想艾特谁一起看呢？', 'video/video11.mp4', 5471, '摄影');
INSERT INTO `movie` VALUES (12, '【小米】小米手表2pro 新品发布 ', 'video/video12.mp4', 3537, '广告');
INSERT INTO `movie` VALUES (13, '【汽车】科尔维特C8是你的爱吗？', 'video/video13.mp4', 32417, '汽车');

-- ----------------------------
-- Table structure for report
-- ----------------------------
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report`  (
  `user_id` int NOT NULL,
  `video_id` int NOT NULL,
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`, `video_id`) USING BTREE,
  INDEX `report_video`(`video_id` ASC) USING BTREE,
  CONSTRAINT `report_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `report_video` FOREIGN KEY (`video_id`) REFERENCES `movie` (`video_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of report
-- ----------------------------
INSERT INTO `report` VALUES (1, 1, '色情');
INSERT INTO `report` VALUES (1, 5, '少儿不宜');

-- ----------------------------
-- Table structure for star
-- ----------------------------
DROP TABLE IF EXISTS `star`;
CREATE TABLE `star`  (
  `user_id` int NOT NULL,
  `video_id` int NOT NULL,
  `date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`, `video_id`) USING BTREE,
  INDEX `star_video`(`video_id` ASC) USING BTREE,
  CONSTRAINT `star_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `star_video` FOREIGN KEY (`video_id`) REFERENCES `movie` (`video_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of star
-- ----------------------------
INSERT INTO `star` VALUES (1, 7, '2022-12-14 20:51:06');
INSERT INTO `star` VALUES (1, 8, '2022-12-14 20:52:06');
INSERT INTO `star` VALUES (1, 10, '2022-12-14 21:01:38');
INSERT INTO `star` VALUES (2, 2, '2022-12-09 11:15:55');
INSERT INTO `star` VALUES (2, 6, '2022-12-09 12:11:57');
INSERT INTO `star` VALUES (2, 8, '2022-12-09 12:13:14');
INSERT INTO `star` VALUES (2, 10, '2022-12-19 11:30:42');
INSERT INTO `star` VALUES (6, 2, '2022-12-11 17:10:09');
INSERT INTO `star` VALUES (6, 12, '2022-12-11 17:50:13');

-- ----------------------------
-- Table structure for upload
-- ----------------------------
DROP TABLE IF EXISTS `upload`;
CREATE TABLE `upload`  (
  `upload_id` int NULL DEFAULT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upload
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` int NULL DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '1', '11', 0);
INSERT INTO `users` VALUES (2, '123456', '123456', 0);
INSERT INTO `users` VALUES (3, '123', '123', 0);
INSERT INTO `users` VALUES (4, '22', '22', 0);
INSERT INTO `users` VALUES (5, '1234', '1234', 0);
INSERT INTO `users` VALUES (6, '333', '333', 0);
INSERT INTO `users` VALUES (7, '44', '44', 0);

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video`  (
  `video_id` int NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` int NULL DEFAULT NULL,
  `label` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`video_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES (1, '《陈翔六点半》努力奋斗摆脱流浪！', 'source/video1.mp4', 233, 'offical');
INSERT INTO `video` VALUES (2, '你见过这么小的奥特曼吗？迷你奥特曼', 'source/video4.mp4', 122, 'offical');
INSERT INTO `video` VALUES (3, '《王者荣耀》：谁说对抗路不能有感情！', 'source/video5.mp4', 122, 'offical');
INSERT INTO `video` VALUES (4, '想我了吗？想我的话怎么不来找我！', 'source/video2.mp4', 245, 'offical');
INSERT INTO `video` VALUES (5, '令人振奋！神舟十四号于今日发射！', 'source/video3.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (6, '请记住他的名字！#D2809动车', 'source/video6.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (7, '我的20岁生日 #广师大 #小赖不耍赖', 'source/video15.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (8, '校园运动会VLOG #阿倩dd', 'source/video11.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (9, '懂事的都已经艾特好兄弟来看了', 'source/video13.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (10, '一次性看完地球46亿年的变化！', 'source/video8.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (11, '四千年一遇的美女 #鞠婧祎', 'source/video9.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (12, '喜欢我眼里的光吗？快艾特你的兄弟来看！', 'source/video10.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (13, '这还不赶紧艾特你的好兄弟来看！', 'source/video14.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (14, '辣吗？不辣我就删了', 'source/video7.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (15, '这是哪里来的球啊？白吗？', 'source/video16.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (16, '短发YYDS！很飒！', 'source/video17.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (17, '《四大美女》#宝藏阁', 'source/video18.mp4', NULL, 'offical');
INSERT INTO `video` VALUES (18, '《就忘了吗 DJ》 #一口甜', 'source/video19.mp4', NULL, 'offical');

SET FOREIGN_KEY_CHECKS = 1;
