package com.videohub.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
    history用于记录用户的访问行为，预测用户偏好
*/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class History {
    private int user_id;
    private int video_id;
    private int times;
}
