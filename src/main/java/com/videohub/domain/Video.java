package com.videohub.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Video {
    private int video_id;
    private String url;
    private String name;
    private int time;
    private String label;

}
