package com.videohub.interceptor;


import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri=request.getRequestURI();
        System.out.println(uri);
        //对用户请求进行判断：是否登录
        if(uri.indexOf("/sign")>=0||uri.indexOf("/toAdmin")>=0){
            return true;
        }
        if(uri.indexOf("/loginAdmin")>=0||uri.indexOf("/error")>=0||uri.indexOf("/readAgreement")>=0){
            return true;
        }
        HttpSession session=request.getSession();
        if(session.getAttribute("usersession")!=null||session.getAttribute("adminsession")!=null){
            return true;
        }
        request.setAttribute("error","您还没有登录，请先登录！");

        request.getRequestDispatcher("/login.jsp").forward(request,response);
        return false;
    }
}
