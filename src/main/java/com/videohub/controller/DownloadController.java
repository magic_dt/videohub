package com.videohub.controller;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.File;
import java.io.IOException;

//下载功能实现
@Controller
public class DownloadController {
   @GetMapping("/download")
   public ResponseEntity<Resource> downloadFile(@RequestParam String filename) throws IOException {
      String PATH_File="C:\\videohub\\src\\main\\webapp\\video";
      File file = new File(PATH_File+filename);
      if (!file.exists()) {
         return ResponseEntity.notFound().build();
      }
      Resource resource = new UrlResource(file.toURI().toURL());
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
      headers.setContentDispositionFormData("attachment", file.getName());
      headers.setContentLength(file.length());
      return ResponseEntity.ok()
              .headers(headers)
              .body(resource);
   }
}