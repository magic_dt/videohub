package com.videohub.controller;

import com.videohub.domain.Video;
import com.videohub.service.impl.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class SearchController {
    @Autowired
    private VideoService videoService;
    @RequestMapping("/search")
    @ResponseBody
    public List<Video> FindVideo(@RequestParam("key") String key){
        List<Video> list=videoService.findVideo(key);
        return list;
    }
}
