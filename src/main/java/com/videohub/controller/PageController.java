package com.videohub.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

//用于页面跳转
@Controller
public class PageController {

    @RequestMapping("/toHot")
    public String toHot(){
        return "hot";
    }

    @RequestMapping("readAgreement")
    public String toAgreement(){
        return "pages/agreement";
    }

    @RequestMapping("/toMain")
    public String toMain(){
        return "index";
    }

    @RequestMapping("/toAccount")
    public String toAccount(){
        return "account";
    }

    @RequestMapping("/watchVideo")
    public String toPlay(){
        return "player";
    }

    @RequestMapping("/error")
    public String error(){
        return "error";
    }

    @RequestMapping("/toNewHot")
    public String toNewHot(){
        return "newHot";
    }

    @RequestMapping("/toSign")
    public String toSign(){
        return "login";
    }

    @RequestMapping("/toSource")
    public String toSource(){
        return "source";
    }

    @RequestMapping("/toAdmin")
    public String toAdmin(){
        return "adminLogin";
    }

    @RequestMapping("/admintor")
    public String admintor(){
        return "admintor";
    }
}
