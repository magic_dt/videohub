package com.videohub.controller;

import com.videohub.domain.Administer;
import com.videohub.service.impl.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes("adminsession")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @RequestMapping("/loginAdmin")
    public ModelAndView loginAdmin(int id,String password){
        ModelAndView modelAndView=new ModelAndView();
        Administer administer=adminService.selectById(id);

        if(administer!=null){
            modelAndView.addObject("adminsession",administer);
            modelAndView.setViewName("admintor");
            return modelAndView;
        }else{
            modelAndView.addObject("msg","非管理员认证信息！");
            modelAndView.setViewName("adminLogin");
            return modelAndView;
        }

    }
}
