package com.videohub.controller;

import com.videohub.domain.History;
import com.videohub.domain.Video;
import com.videohub.service.impl.HistoryService;
import com.videohub.service.impl.StarService;
import com.videohub.service.impl.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import java.util.List;

@Controller
public class VideoController {
    @Autowired
    private VideoService videoService;
    //历史记录
    @Autowired
    private HistoryService historyService;

    @RequestMapping("/allVideo")
    @ResponseBody
    public List<Video> AllVideo(){
        List<Video> list=videoService.selectAll();
        return list;
    }

    //player页面发送请求，返回渲染 访客量+1
    @RequestMapping("/playVideo")
    @ResponseBody
    public Video findVideoById(int user_id,int video_id){
        //记录用户浏览行为
        History ht=historyService.findHistory(user_id, video_id);
        if(ht!=null){
            int time=ht.getTimes();
            ht.setTimes(time+1);
            historyService.updateHistory(ht);
        }else {
            History history=new History();
            history.setUser_id(user_id);
            history.setVideo_id(video_id);
            history.setTimes(1);
            historyService.addHistory(history);
        }
        Video video=videoService.findVideoById(video_id);
        //将浏览量返回加一
        int time=video.getTime()+1;
        videoService.updateVideoById(time,video_id);
        return video;
    }

    @RequestMapping("/deleteVideo")
    @ResponseBody
    public String deleteVideo(int video_id){
        videoService.deleteVideoById(video_id);
        return "successful";
    }
}
