package com.videohub.controller;

import com.videohub.domain.Report;
import com.videohub.service.impl.ReportService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ReportController {
    @Autowired
    private ReportService reportService;

    @RequestMapping("/submitReport")
    @ResponseBody
    public String submitReport(Report report){
        reportService.submitReport(report);
        return "successful";
    }

    @RequestMapping("/getReport")
    @ResponseBody
    public List<Report> getReport(){
        return reportService.allReport();
    }

    @RequestMapping("delReport")
    @ResponseBody
    public String delReport(int user_id, int video_id){
        reportService.delReport(user_id, video_id);
        return "successful";
    }

}
