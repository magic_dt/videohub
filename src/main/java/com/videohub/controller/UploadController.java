package com.videohub.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

//上传功能还缺少上传页面
@Controller
@RequestMapping(value = "/upload")
public class UploadController {

    //methods
    @RequestMapping(value = "/file")
    @ResponseBody
    public Map<String,Object> uploadFile(@RequestParam(value = "file") MultipartFile file,
                                         @RequestParam(value = "username") String username,
                                         HttpServletRequest request){
        System.out.println(file);
        System.out.println("username="+username);
        //获取protocol通信协议
        String scheme = request.getScheme();// http
        //获取服务名称
        String serverName = request.getServerName();//localhost
        //获取端口号
        int serverPort = request.getServerPort();
        //contextPath
        String contextPath = request.getContextPath();// /stasys_v3
        String url=scheme+"://"+serverName+":"+serverPort+contextPath+"/upload/";
        Map<String,Object> map=new HashMap<>();
        // 控制文件大小
        if(file.getSize()>1024*1024*512){
            map.put("status",false);
            map.put("message", "文件大小不能超过512M");
            map.put("url",null);
            return map;
        }
        try {
            //保存文件到当前项目根目录下
            String realPath = request.getServletContext().getRealPath("/source");
            File dir=new File(realPath);

            //判断目录是否存在
            if (!dir.exists()){
                dir.mkdirs();//不存在-创建新目录
            }
            //获取文件名
            String originalFilename = file.getOriginalFilename();
            //使用UUID替换文件名——避免文件名冲突
            String uuid= UUID.randomUUID().toString();
            //获取文件拓展名
            String extendsName = originalFilename.substring(originalFilename.lastIndexOf("."));
            //  控制文件类型
            if(!extendsName.equals(".mp4")){
                map.put("status",false);
                map.put("message", "文件类型错误,必须为.zip压缩文件");
                map.put("url",null);
                return map;
            }
            //新的文件名-056af4c8-2a5a-4e6e-a108-45f689865264.zip
            String newFileName=uuid.concat(extendsName);
            //文件对应URL路径：http://localhost:8010/stasys_v3/upload056af4c8-2a5a-4e6e-a108-45f689865264.zip

            String fileUrl = url.concat(newFileName);
            //文件保存位置
            File saveLoc = new File(dir,newFileName);
            //保存文件
            file.transferTo(saveLoc);
            //填充返回值
            map.put("status",true);
            map.put("msg","文件上传成功!");
            map.put("url","");
        }catch (Exception e){
            e.printStackTrace();
            map.put("status",false);
            map.put("msg","文件上传失败!");
            map.put("url","ddd");
        }
        System.out.println(map.toString());
        return map;
    }
}


