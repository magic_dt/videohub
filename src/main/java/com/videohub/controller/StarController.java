package com.videohub.controller;

import com.videohub.domain.Star;
import com.videohub.domain.Video;
import com.videohub.service.impl.StarService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class StarController {
    @Autowired
    private StarService starService;
    @RequestMapping("/Star")
    @ResponseBody
    public void Star(int user_id,int video_id){
        starService.addStar(user_id, video_id);
    }

    @RequestMapping("/RmStar")
    @ResponseBody
    public void RmStar(int user_id,int video_id){
        starService.rmStar(user_id, video_id);
    }

    @RequestMapping("/getUserStar")
    @ResponseBody
    public List<Star> findStarById(int user_id){
        return starService.findStarById(user_id);
    }

    @RequestMapping("/userStar")
    @ResponseBody
    public List<Video> userStar(int user_id){
        return starService.findUserStar(user_id);
    }
}
