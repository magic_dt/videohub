package com.videohub.controller;

import com.videohub.domain.User;
import com.videohub.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@SessionAttributes("usersession")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/sign")
    public ModelAndView Login(String username, String password, Model model) {
        //先找username是否存在，如果存在则对比密码是否一致，如果不存在，则创建用户及密码
        //判断使用 findUserByUsername() 和 creatUser()
        ModelAndView modelAndView = new ModelAndView();
        User u = userService.findUserByUsername(username);
        if (u != null) {
            if (u.getPassword().equals(password)) {
                modelAndView.addObject("usersession", u);
                model.addAttribute("username",username);
                modelAndView.setViewName("hot");
                return modelAndView;
            } else {
                //modelAndView.addObject("msg","密码错误！");
                model.addAttribute("msg","密码错误！");
                modelAndView.setViewName("login");
                return modelAndView;
            }
        } else {
            User newu = new User();
            newu.setUsername(username);
            newu.setPassword(password);
            userService.createUser(newu);
            modelAndView.addObject("usersession", newu);
            model.addAttribute("msg","注册成功，点击继续登录");
            modelAndView.setViewName("login");
            return modelAndView;
        }
    }

    //获取所有用户
    @RequestMapping("/allUser")
    @ResponseBody
    public List<User> selectAll(){
        return userService.selectAll();
    }

}
