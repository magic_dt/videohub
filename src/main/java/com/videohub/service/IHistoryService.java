package com.videohub.service;

import com.videohub.domain.History;
import org.apache.ibatis.annotations.Param;

public interface IHistoryService {
    History findHistory(@Param("user_id") int user_id, @Param("video_id") int video_id);
    void addHistory(History history);
    void updateHistory(History history);
}
