package com.videohub.service;

import com.videohub.domain.Star;
import com.videohub.domain.Video;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IStarService {
    void addStar(@Param("user_id") int user_id, @Param("video_id") int video_id);
    void rmStar(@Param("user_id") int user_id,@Param("video_id") int video_id);
    List<Star> findStarById(int user_id);
    List<Video> findUserStar(int user_id);
}
