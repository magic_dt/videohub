package com.videohub.service;

import com.videohub.domain.Report;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IReportService {
    void submitReport(Report report);
    List<Report> allReport();
    void delReport(@Param("user_id") int user_id, @Param("video_id") int video_id);
}
