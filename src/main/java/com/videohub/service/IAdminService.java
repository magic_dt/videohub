package com.videohub.service;

import com.videohub.domain.Administer;

public interface IAdminService {
    Administer selectById(int id);
}
