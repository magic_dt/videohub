package com.videohub.service;

import com.videohub.domain.Video;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IVideoService {
    List<Video> selectAll();
    List<Video> findVideo(String key);
    Video findVideoById(Integer video_id);
    void updateVideoById(@Param("time") int time, @Param("video_id") int video_id);
    void deleteVideoById(int video_id);
}
