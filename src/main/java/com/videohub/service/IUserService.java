package com.videohub.service;


import com.videohub.domain.User;

import java.util.List;

public interface IUserService {
    void createUser(User user);
    User findUserByUsername(String username);
    List<User> selectAll();
}
