package com.videohub.service.impl;

import com.videohub.domain.History;
import com.videohub.mapper.HistoryMapper;
import com.videohub.service.IHistoryService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryService implements IHistoryService {
    @Autowired
    private HistoryMapper historyMapper;

    @Override
    public History findHistory(@Param("user_id") int user_id, @Param("video_id") int video_id){
        return historyMapper.findHistory(user_id, video_id);
    }

    @Override
    public void addHistory(History history){
        historyMapper.addHistory(history);
    }

    @Override
    public void updateHistory(History history){
        historyMapper.updateHistory(history);
    }
}
