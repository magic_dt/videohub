package com.videohub.service.impl;

import com.videohub.domain.Administer;
import com.videohub.mapper.AdminMapper;
import com.videohub.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService implements IAdminService {
    @Autowired
    private AdminMapper adminMapper;
    @Override
    public Administer selectById(int id){
        return adminMapper.selectById(id);
    }
}
