package com.videohub.service.impl;

import com.videohub.domain.Report;
import com.videohub.mapper.ReportMapper;
import com.videohub.service.IReportService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportService implements IReportService {
    @Autowired
    private ReportMapper reportMapper;
    @Override
    public void submitReport(Report report){
        reportMapper.submitReport(report);
    }
    @Override
    public List<Report> allReport(){
        return reportMapper.allReport();
    }
    @Override
    public void delReport(@Param("user_id") int user_id, @Param("video_id") int video_id){
        reportMapper.delReport(user_id, video_id);
    }
}
