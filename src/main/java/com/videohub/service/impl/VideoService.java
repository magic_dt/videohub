package com.videohub.service.impl;

import com.videohub.domain.Video;
import com.videohub.mapper.VideoMapper;
import com.videohub.service.IVideoService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoService implements IVideoService {
    @Autowired
    private VideoMapper videoMapper;
    @Override
    public List<Video> selectAll(){
        return videoMapper.selectAll();
    }
    @Override
    public List<Video> findVideo(String key){
        return videoMapper.findVideo(key);
    }
    @Override
    public Video findVideoById(Integer video_id){
        return videoMapper.findVideoById(video_id);
    }
    @Override
    public void updateVideoById(@Param("time") int time, @Param("video_id") int video_id){
        videoMapper.updateVideoById(time,video_id);
    }
    @Override
    public void deleteVideoById(int video_id){
        videoMapper.deleteVideoById(video_id);
    }
}
