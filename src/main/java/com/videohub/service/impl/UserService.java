package com.videohub.service.impl;

import com.videohub.domain.User;
import com.videohub.mapper.UserMapper;
import com.videohub.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public void createUser(User user){
        userMapper.createUser(user);
    }

    @Override
    public User findUserByUsername(String username){
        return userMapper.findUserByUsername(username);
    }

    @Override
    public List<User> selectAll(){
        return userMapper.selectAll();
    }
}
