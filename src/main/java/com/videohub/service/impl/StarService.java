package com.videohub.service.impl;

import com.videohub.domain.Star;
import com.videohub.domain.Video;
import com.videohub.mapper.StarMapper;
import com.videohub.service.IStarService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StarService implements IStarService {
    @Autowired
    private StarMapper starMapper;

    @Override
    public void addStar(@Param("user_id") int user_id, @Param("video_id") int video_id){
        starMapper.addStar(user_id,video_id);
    }

    @Override
    public void rmStar(@Param("user_id") int user_id,@Param("video_id") int video_id){
        starMapper.rmStar(user_id, video_id);
    }

    @Override
    public List<Star> findStarById(int user_id){
        return starMapper.findStarById(user_id);
    }

    @Override
    public List<Video> findUserStar(int user_id){
        return starMapper.findUserStar(user_id);
    }
}
