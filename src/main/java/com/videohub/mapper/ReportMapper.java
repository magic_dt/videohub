package com.videohub.mapper;

import com.videohub.domain.Report;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ReportMapper {
    void submitReport(Report report);
    List<Report> allReport();
    void delReport(@Param("user_id") int user_id,@Param("video_id") int video_id);
}
