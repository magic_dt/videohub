package com.videohub.mapper;

import com.videohub.domain.History;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface HistoryMapper<addHistory> {
    History findHistory(@Param("user_id") int user_id,@Param("video_id") int video_id);
    void addHistory(History history);
    void updateHistory(History history);
}
