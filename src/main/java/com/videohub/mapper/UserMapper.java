package com.videohub.mapper;


import com.videohub.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    void createUser(User user);
    User findUserByUsername(String username);
    List<User> selectAll();
}
