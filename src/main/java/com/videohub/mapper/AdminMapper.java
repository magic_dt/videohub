package com.videohub.mapper;

import com.videohub.domain.Administer;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper {
    Administer selectById(int id);
}
