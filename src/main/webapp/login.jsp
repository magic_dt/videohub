<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<html class="element">
<head>
    <title>VIDEO•HUB--匿名私人視頻網站</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/login.css" rel="stylesheet">
    <link rel="icon" sizes="90x90" href="imgs/logo.png">
    <!--引入阿里巴巴圖標庫JS-->
    <script src="js/iconfont.js"></script>
    <script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>
<header>
    <a href="index.jsp"><img class="logo" src="imgs/logo.png"><strong>IDEO•HUB</strong></a>
    <a href="${pageContext.request.contextPath}/toAdmin" title="管理员登录"><svg t="1670986359185" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3069" width="32" height="32"><path d="M746.88 695.68a40.32 40.32 0 0 0-39.68 42.24 40.96 40.96 0 0 0 81.92 0 41.6 41.6 0 0 0-42.24-40.96z m102.4-174.08a34.56 34.56 0 0 1 29.44 16.64l102.4 184.32a31.36 31.36 0 0 1 0 30.08l-102.4 184.32a33.28 33.28 0 0 1-29.44 16.64h-203.52a34.56 34.56 0 0 1-28.8-16.64L512 752.64a31.36 31.36 0 0 1 0-30.08l104.96-184.32a32.64 32.64 0 0 1 28.8-16.64z m-384-448a238.72 238.72 0 0 1 243.2 234.24 240 240 0 0 1-238.72 234.88 30.72 30.72 0 0 1-21.76 7.04H353.92A270.08 270.08 0 0 0 101.12 832v6.4a46.72 46.72 0 0 0 40.96 48.64H492.8a32 32 0 0 1 31.36 32 32.64 32.64 0 0 1-31.36 31.36H143.36A108.8 108.8 0 0 1 39.04 838.4V832a337.28 337.28 0 0 1 273.28-341.76 233.6 233.6 0 0 1-89.6-183.04 239.36 239.36 0 0 1 242.56-236.8z m366.08 512h-167.04l-83.84 153.6 83.84 152.32H832l83.84-152.96L832 584.32z m-84.48 48.64a103.68 103.68 0 1 1-106.88 102.4 104.96 104.96 0 0 1 106.24-103.68z m-281.6-499.2A175.36 175.36 0 0 0 288 307.2a177.28 177.28 0 0 0 352 0 174.72 174.72 0 0 0-174.08-173.44z" fill="#1296db" p-id="3070"></path></svg></a>
</header>
<hr>
<main>
    <div class="form-area">
        <div class="error">
            <b>${msg}</b>
        </div>
        <form class="form" action="sign" method="post">
            <div class="InputBox">
                <input type="text" required="required" name="username" id="username">
                <span>User Name</span>
            </div>
            <div class="InputBox">
                <input type="password" required="required" name="password" id="password">
                <span>Password</span>
            </div>
            <div class="agreement">
                <input type="checkbox" class="agree" name="agree" id="agreement">
                <label for="agreement">我已阅读并同意<b class="read" onclick="newtab()" title="用户协议">《用户协议》</b></label>
            </div>
            <div>
                <button class="unchecked" type="submit" id="submit" name="submit" onclick="isAgree()" disabled>注冊&登錄</button>
            </div>
            <div class="forget">
                <a href="#" title="忘記密碼">Forget the password?</a>
            </div>
        </form>
    </div>
</main>
<hr>
<footer>
    <div class="container">
        <p>
            地點：廣州市天河區中山大道西293號<br>
            郵編：510665; 傳真：020-38257901<br>
            電話：020-38256601
        </p>
        <br>
        <img class="bt-logo" src="imgs/bottom.png">
        <br><b>Copyright@ 2022 All Rights Reserved.</b>
    </div>
</footer>

<script>
        //监听输入框是否有值以判断是否禁用button
        $("#username,#password").on('input propertychange',function(){
            if($('#username').val() !== "" && $('#password').val().replace(/(^\s*)|(\s*$)/g, "")){
                $("#submit").removeClass("unchecked");
                document.getElementById("submit").disabled = false;
            }
            else {
                $("#submit").addClass("unchecked");
                document.getElementById("submit").disabled = true;
            }
        });


    //判断是否勾选同意协议
    function isAgree(){
        is_agree = $("#agreement").prop("checked");
        if(is_agree){
            //如果同意则提交
            $("#submit").submit();
        }else{
            //不同意则阻止提交且提示
            event.preventDefault();
            alert("请先同意《用户协议》！")
        }
    }

    //用户协议
    function newtab(){
        window.open("${pageContext.request.contextPath}/readAgreement");
    }

    <%--$(window).ready(function (){--%>
    <%--    $("#username").val(${usersession.username});--%>
    <%--    $("#password").val(${usersession.password});--%>
    <%--})--%>

</script>

</body>
</html>
