<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en" class="element">
<head>
  <title>VIDEO•HUB--Player</title>
  <link rel="icon" sizes="90x90" href="imgs/logo.png">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--Bootstrap与JQuery-->
  <link
          href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.css"
          rel="stylesheet">
  <link
          href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.min.css"
          rel="stylesheet">
  <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script>
  <link
          href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap-utilities.rtl.css"
          rel="stylesheet">
  <!--CSS-->
  <link href="css/player.css" rel="stylesheet">
  <!--Player JS-->
  <script src="js/playerjs.js" type="text/javascript"></script>
  <script src="js/iconfont.js"></script>
  <style>
    .icon {
      width: 1em;
      height: 1em;
      vertical-align: -0.15em;
      fill: currentColor;
      overflow: hidden;
    }
  </style>
</head>
<body>

<!--BootStrap导航栏-->
<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light" id="nav">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      <img src="imgs/logo.png" alt="" width="32" height="28"
           class="d-inline-block align-text-top">
      <strong>IDEO•HUB</strong>
    </a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-4 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="${pageContext.request.contextPath}/toMain">HOME</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="${pageContext.request.contextPath}/toNewHot">HOT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#" id="mode" onclick="changeMode();" title="日夜间模式">LIGHT</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/toAccount">ACCOUNT</a>
        </li>
      </ul>
      <div class="d-flex" role="search" action="#">
        <!-- <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search"> -->
        <input class="search" type="search" name="key" id="key" placeholder="搜索...">
        <button class="btn btn-outline-success" id="search" onclick="search()">Search</button>
      </div>
    </div>
  </div>
</nav>

<div class="video">
  <div id="player" class="player">
  </div>
  <div class="video-name" id="info">

  </div>
  <div class="video-control">
    <button type="button" class="btn btn-outline-primary" id="download">
      <svg t="1670493957270" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3227" width="32" height="32">
        <path d="M624 706.3h-74.1V464c0-4.4-3.6-8-8-8h-60c-4.4 0-8 3.6-8 8v242.3H400c-6.7 0-10.4 7.7-6.3 12.9l112 141.7c3.2 4.1 9.4 4.1 12.6 0l112-141.7c4.1-5.2 0.4-12.9-6.3-12.9z" p-id="3228"></path><path d="M811.4 366.7C765.6 245.9 648.9 160 512.2 160S258.8 245.8 213 366.6C127.3 389.1 64 467.2 64 560c0 110.5 89.5 200 199.9 200H304c4.4 0 8-3.6 8-8v-60c0-4.4-3.6-8-8-8h-40.1c-33.7 0-65.4-13.4-89-37.7-23.5-24.2-36-56.8-34.9-90.6 0.9-26.4 9.9-51.2 26.2-72.1 16.7-21.3 40.1-36.8 66.1-43.7l37.9-9.9 13.9-36.6c8.6-22.8 20.6-44.1 35.7-63.4 14.9-19.2 32.6-35.9 52.4-49.9 41.1-28.9 89.5-44.2 140-44.2s98.9 15.3 140 44.2c19.9 14 37.5 30.8 52.4 49.9 15.1 19.3 27.1 40.7 35.7 63.4l13.8 36.5 37.8 10C846.1 454.5 884 503.8 884 560c0 33.1-12.9 64.3-36.3 87.7-23.4 23.4-54.5 36.3-87.6 36.3H720c-4.4 0-8 3.6-8 8v60c0 4.4 3.6 8 8 8h40.1C870.5 760 960 670.5 960 560c0-92.7-63.1-170.7-148.6-193.3z" p-id="3229">
        </path>
      </svg>
      Download
    </button>
    <button type="button" class="btn btn-outline-danger" id="like">
      <svg t="1670493586735" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="7084" width="32" height="32">
        <path d="M512 948.246261c-30.185739 0-58.902261-11.553391-80.851478-32.567652l-3.027478-2.938435C310.984348 797.985391 103.846957 589.824 35.684174 487.802435 13.022609 453.854609 0 408.754087 0 364.076522 0 205.089391 129.335652 75.753739 288.322783 75.753739c87.841391 0 169.494261 39.735652 223.677217 106.406957 54.182957-66.693565 135.835826-106.406957 223.677217-106.406957C894.664348 75.753739 1024 205.089391 1024 364.076522c0 44.699826-13.04487 89.800348-35.706435 123.725913-68.162783 102.021565-275.255652 310.138435-392.637217 425.115826l-2.82713 2.80487C570.902261 936.69287 542.185739 948.246261 512 948.246261zM288.322783 145.385739c-120.58713 0-218.690783 98.103652-218.690783 218.690783 0 31.254261 8.726261 62.241391 23.952696 85.036522 67.094261 100.396522 290.57113 323.049739 383.532522 414.118957 22.149565 21.303652 49.753043 19.18887 67.584 2.114783 95.142957-93.228522 318.597565-315.859478 385.691826-416.233739 15.181913-22.77287 23.908174-53.76 23.908174-85.036522 0-120.58713-98.081391-218.690783-218.646261-218.690783-80.851478 0-154.802087 44.388174-192.957217 115.845565L512 318.73113l-30.72-57.499826C443.12487 189.773913 369.174261 145.385739 288.322783 145.385739z" p-id="7085">
        </path>
      </svg>
      Like
    </button>
    <button type="button" class="btn btn-outline-secondary" id="report">
      <svg t="1670494266618" class="icon" viewBox="0 0 1189 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="7432" width="32" height="32">
        <path d="M594.137507 0c199.190609 344.004472 395.519285 682.285075 594.137507 1024-397.236445 0-790.466182 0-1188.275014 0C198.045836 682.285075 394.946898 343.432085 594.137507 0zM544.912241 645.65232c33.770822 0 65.824483 0 98.450531 0 0-70.975964 0-140.234768 0-209.493572-33.770822 0-65.252096 0-98.450531 0C544.912241 505.989939 544.912241 574.676356 544.912241 645.65232zM545.484628 757.267747c0 35.487982 0 69.258804 0 103.029625 33.198435 0 65.252096 0 97.878144 0 0-34.915595 0-68.686417 0-103.029625C610.164338 757.267747 578.683063 757.267747 545.484628 757.267747z" p-id="7433">
        </path>
      </svg>
      Report
    </button>

  </div>
</div>


<div class="report-text" id="report-text" style="display: none;">
  <h1>举报原因</h1>
  <form>
    <textarea rows="8" cols="32" placeholder="请输入举报原因" id="report-reason"></textarea>
    <span><button type="button" class="btn btn-secondary" id="back" style="width: 90%;">Cancel</button></span>
    <span><button type="button" class="btn btn-danger" id="submit" style="width: 90%;">Report</button></span>
  </form>
</div>

<div id="ly" style="position:absolute;top:0px;opacity: 0.5;background-color:#777; z-index:99998; left: 0px;display:none; width: 100%;height: 100%;"></div>

<footer>
  <div class="container">
    <p>
      地點：廣州市天河區中山大道西293號<br>
      郵編：510665; 傳真：020-38257901<br>
      電話：020-38256601
    </p>
    <br><b>Copyright@ 2022 All Rights Reserved.</b>
  </div>
</footer>


<!--Player JS-->
<script>

  var Mode=1;
  //获取视频id
  var u=window.localStorage.getItem('video_id');
  // console.log(u);
  var player = new Playerjs({id:"player", file:"video/video"+u+".mp4"});

  //日夜间模式
  function changeMode(){
    $("body").toggleClass("dark");
    $("#nav").toggleClass("navbar-light bg-light");
    $("#nav").toggleClass("navbar-dark bg-dark");
    if(Mode==0){
      $("#mode").html("LIGHT");
      Mode=1;
    }else{
      $("#mode").html("DARK");
      Mode=0;
    }
  }

  //渲染页面
  $(window).ready(function (){
    $.post("${pageContext.request.contextPath}/playVideo",
            {
              user_id:${sessionScope.usersession.id},
              video_id:u
            },function (res){
        //判断浏览量是否大于21000
        if(res.time>21000){
           var lab="<svg t='1670465389973' viewBox='0 0 1024 1024' \n"+
          "version='1.1' xmlns='http://www.w3.org/2000/svg' p-id='5217 \n"+
          "width='32' height='32'><path d='M414.72 \n"+
          "981.333s-416.427-91.52-230.4-545.92c0 0 42.667 50.56 36.48 74.88 0 \n"+
          "0 33.067-114.773 104.533-183.253 61.44-59.093 123.734-224.64 \n"+
          "66.347-284.373 0 0 284.8 59.733 316.587 359.04 0 0 36.48-95.36 \n"+
          "111.146-104.747a204.587 204.587 0 0 0 0 130.987s235.947 \n"+
          "403.84-170.666 540.373c0 0 121.813-138.453-136.534-375.893 0 \n"+
          "0-61.013 128-97.28 171.946-0.213 0-101.973 114.134-0.213 216.96z '"+
          "p-id='5218' fill='#ea1c25'></path></svg>\n";
           var hot="hot";
        }else {
          var lab="";
          var hot="";
        }
        $("#info").append(lab+"<b class='intro "+hot+"'>"+res.name+"</b>"+
                "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16'"+
                "fill='currentColor' class='bi bi-badge-hd-fill' viewBox='0 0\n"+
                "16 16'>\n"+
                "<path d='M10.53 5.968h-.843v4.06h.843c1.117 0 1.622-.667\n"+
                "1.622-2.02 0-1.354-.51-2.04-1.622-2.04z'/>\n"+
                "<path d='M2 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h12a2 2 0 0 0\n"+
                "2-2V4a2 2 0 0 0-2-2H2zm5.396\n"+
                "3.001V11H6.209V8.43H3.687V11H2.5V5.001h1.187v2.44h2.522V5h1.187zM8.5\n"+
                "11V5.001h2.188c1.824 0 2.685 1.09 2.685 2.984C13.373 9.893\n"+
                "12.5 11 10.69 11H8.5z'/>\n"+
                "</svg>");
    })
  })

  //简单的实现下载功能
  $("#download").click(function (){
    window.location.href ="${pageContext.request.contextPath}/download"+"?filename=/video"+u+".mp4";
    <%--$.get("${pageContext.request.contextPath}/download"),--%>
    <%--        {--%>
    <%--          filename:"video/video"+u+".mp4"--%>
    <%--        }--%>
  })

  var like=0;
  //获取用户收藏列表 更新状态
  $(window).ready(function (){
    $.get("${pageContext.request.contextPath}/getUserStar",
            {
              user_id:${sessionScope.usersession.id}
            },function (res){
              let i=0;
              console.log(res[0].video_id);
              for(i=0;i<res.length;i++){
                  if(res[i].video_id==u){
                    $("#like").addClass("active");
                    like=1;
                  }
              }
            }
    )
  })

  //用于判断是否收藏
  $("#like").click(function (){
    console.log(like);
    $("#like").toggleClass("active");
    if(like == 0){
        like=1;
        $.post("${pageContext.request.contextPath}/Star",
              {
                user_id:${sessionScope.usersession.id},
                video_id:u
              },function (){

              }
      )
    }else{
        like=0;
        $.post("${pageContext.request.contextPath}/RmStar",
              {
                user_id:${sessionScope.usersession.id},
                video_id:u
              },function (){

              }
        )
    }
  })




  var report=0;
  var el = document.getElementsByTagName('body')[0];
  //举报框弹出
  $("#report").click(function(){
    if(report==0){
      const p=document.getElementById("report-text");
      const cover=document.getElementById("ly");
      cover.style.display="block";
      p.style.display="block";
      el.style.overflow = "hidden";
    }else {
      alert("您提交的举报信息已经提交给网站管理员了，请勿重复举报！")
    }
  })

  //举报框退回
  $("#back").click(function(){
    const p=document.getElementById("report-text");
    const cover=document.getElementById("ly");
    cover.style.display="none";
    p.style.display="none";
    el.style.overflow = "auto";
  })

  //提交举报
  $("#submit").click(function (){
    //避免重复举报
    report=1;
    let reason=$("#report-reason").val();
    $.post("${pageContext.request.contextPath}/submitReport",
            {
              user_id:${sessionScope.usersession.id},
              video_id:u,
              reason:reason
            },function (res){
                console.log(res);
            }
    )
    //将举报框设为不显示
    const cover=document.getElementById("ly");
    cover.style.display="none";
    const p=document.getElementById("report-text");
    p.style.display="none";
    el.style.overflow = "auto";
    //延时1秒 告知用户举报成功
    setTimeout(function (){
      alert("你提供的举报信息已提交给网站管理员了，感谢反馈！");
    },1000);
  })

</script>
</body>
</html>
