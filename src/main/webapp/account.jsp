<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html class="element">
<head>
    <title>VIDEO•HUB--用户中心</title>
    <meta charset="UTF-8">
    <link rel="icon" sizes="90x90" href="imgs/logo.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Bootstrap与JQuery-->
    <link
            href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.css"
            rel="stylesheet">
    <link
            href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.min.css"
            rel="stylesheet">
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script>
    <link
            href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap-utilities.rtl.css"
            rel="stylesheet">

    <!--css-->
    <link href="css/newhot.css" rel="stylesheet">
    <!--fonticon 库-->
    <script src="js/iconfont.js"></script>
    <style>
        .area{
            position: relative;
            top: 80px;
            text-align: center;
        }
        /*去除滚动条*/
        .element::-webkit-scrollbar {
            display: none; /* Chrome Safari */
        }

    </style>
</head>
<body class="dark">
<!--BootStrap导航栏-->
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" id="nav">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="imgs/logo.png" alt="" width="32" height="28"
                 class="d-inline-block align-text-top">
            <strong>IDEO•HUB</strong>
        </a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-4 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="${pageContext.request.contextPath}/toMain">HOME</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="${pageContext.request.contextPath}/toNewHot">HOT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" id="mode" onclick="changeMode();" title="日夜间模式">LIGHT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/toAccount">ACCOUNT</a>
                </li>
            </ul>
            <div class="d-flex" role="search" action="#">
                <!-- <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search"> -->
                <input class="search" type="search" name="key" id="key" placeholder="搜索...">
                <button class="btn btn-outline-success" id="search" onclick="search()">Search</button>
            </div>
        </div>
    </div>
</nav>

<div class="area">
    <h1>我的收藏</h1>
</div>
<!--视频内容-->
<main>
    <div class="container">
        <div class="row row-cols-3" id="video-area">

        </div>
    </div>
</main>
<script>
    //发送get请求 后端获取数据
    $(window).ready(function (){
        $.get("${pageContext.request.contextPath}/userStar",
            {
                user_id:${sessionScope.usersession.id}
            }
            ,function (result){
                for(let i=0; i<result.length; i++){
                    //浏览量高于21000，标红处理
                    if(result[i].time>21000){
                        var t="class='link hot dark'";
                    }else {
                        var t="class='link dark'";
                    }
                    $("#video-area").append("<div class='col'>"+
                        "<div class='video-card'>\n"+
                        "<video name='video' id='video' onmouseover='videoPlayback("+i+")' onmouseout='videoStopped("+i+")'>"+
                        "<source src="+result[i].url+" type='video/mp4'>"+
                        "</video>"+
                        "<div class='video-name'>"+
                        "<a href='javascript:void(0);' "+t+"onclick='toPlay("+result[i].video_id+")'></b>"+
                        "<svg t='1670324538380' viewBox='0 0 1024 1024' \n"+
                        "version='1.1' xmlns='http://www.w3.org/2000/svg' p-id='3533'\n"+
                        " width='20px' height='20px'><path d='M964.720538 170.38988a33.214402\n"+
                        " 33.214402 0 0 0-9.964321 0 369.344145 369.344145 0 0 1-103.961077\n"+
                        " 14.946481A370.008433 370.008433 0 0 1 538.247622 14.946481a33.214402\n"+
                        " 33.214402 0 0 0-54.803763 0 370.340577 370.340577 0 0 1-312.879663\n"+
                        " 171.718456 370.672721 370.672721 0 0 1-101.968212-14.282193 33.214402\n"+
                        " 33.214402 0 0 0-9.632177 0 33.214402 33.214402 0 0 0-33.214401\n"+
                        " 33.214402v332.144015c0 265.715212 365.358417 486.258839 486.258838\n"+
                        " 486.258839 94.3289 0 486.258839-218.882906 486.258839-486.258839V203.272138a33.214402\n"+
                        " 33.214402 0 0 0-33.546545-32.882258z m-186.664937 259.072332l-265.715213\n"+
                        " 227.850795a33.214402 33.214402 0 0 1-43.178722 0 30.225105 30.225105 0\n"+
                        " 0 1-4.317872-2.657152l-143.818359-136.179047a33.214402 33.214402 0 0\n"+
                        " 1 45.503731-48.160882L490.418883 587.562764l242.797276-208.254298a33.214402\n"+
                        " 33.214402 0 1 1 43.510866 50.153746z' fill='#22a6ef' p-id='3534'></path></svg><b>"
                        +" "+result[i].name+"</a>"+
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16'"+
                        "fill='currentColor' class='bi bi-badge-hd-fill' viewBox='0 0\n"+
                        "16 16'>\n"+
                        "<path d='M10.53 5.968h-.843v4.06h.843c1.117 0 1.622-.667\n"+
                        "1.622-2.02 0-1.354-.51-2.04-1.622-2.04z'/>\n"+
                        "<path d='M2 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h12a2 2 0 0 0\n"+
                        "2-2V4a2 2 0 0 0-2-2H2zm5.396\n"+
                        "3.001V11H6.209V8.43H3.687V11H2.5V5.001h1.187v2.44h2.522V5h1.187zM8.5\n"+
                        "11V5.001h2.188c1.824 0 2.685 1.09 2.685 2.984C13.373 9.893\n"+
                        "12.5 11 10.69 11H8.5z'/>\n"+
                        "</svg>"+
                        "<div class='video-info'>"+"浏览量："+result[i].time+"</div>"+
                        "</div>");
                }
            })
    })

    //全局变量，监测日夜间模式
    var Mode=1;

    //视频鼠标监测播放
    //鼠标移进去
    function videoPlayback(n){
        //获取视频标签
        let video = document.getElementsByName("video");
        //给视频标签添加缓存播放---video标签属性
        video[n].setAttribute("autoplay","autoplay");
        //给视频标签添加循环播放---video标签属性
        video[n].setAttribute("loop","loop");
        //添加视频控件
        video[n].setAttribute("controls","true");
        //禁用原始下载功能
        video[n].setAttribute("controlslist","nodownload noremoteplayback");
        //播放视频
        video[n].play();
    }
    //鼠标离开
    function videoStopped(n){
        //获取视频标签
        let video = document.getElementsByName("video");
        //移除控件
        video[n].removeAttribute("controls");
        //停止播放
        video[n].pause();
    }


    //悬停时间超4秒全屏播放



    //日夜间模式
    function changeMode(){
        $("body").toggleClass("dark");
        $("#nav").toggleClass("navbar-light bg-light");
        $("#nav").toggleClass("navbar-dark bg-dark");
        $("#totop").toggleClass("dark");
        $("a.link").toggleClass("dark");
        if(Mode==0){
            $("#mode").html("LIGHT");
            Mode=1;
        }else{
            $("#mode").html("DARK");
            Mode=0;
        }
    }

    //记录在本地会话 跨页面传参
    function toPlay(n){
        //n为video_id
        window.localStorage.setItem('video_id', n);
        //新开一个标签页播放
        <%--window.open("${pageContext.request.contextPath}/watchVideo");--%>
        //直接跳转
        window.location.href="${pageContext.request.contextPath}/watchVideo";
    }

</script>
</body>
</html>
