<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="GBK" isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Video-MAX_Login</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800|Roboto:400,500,700" rel="stylesheet">
    <!-- Theme CSS -->
    <link type="text/css" href="bootstrap/css/theme.css" rel="stylesheet">
    <!-- Demo CSS - No need to use these in your project -->
    <link type="text/css" href="bootstrap/css/demo.css" rel="stylesheet">
</head>
<body style="background: black;">
<nav class="navbar navbar-expand-lg navbar-transparent navbar-dark bg-dark py-4">
    <div class="container">
        <a class="navbar-brand" href=""
           style="position: absolute; left: 40px; top:30px; font-size: xx-large;"><strong>VIDEO</strong>-MAX</a>
    </div>
</nav>
<!--此处引用了boostrap模板，并经过CSS修改拼接-->
<!--登录页面与注册页面共用逻辑-->
<main class="main">
    <section class="py-xl bg-cover bg-size--cover">
        <span class="mask bg-primary alpha-6"></span>
        <div class="container d-flex align-items-center no-padding">
            <div class="col">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="card bg-primary text-white">
                            <div class="card-body">
                                <a href="index.jsp">
                                    <button type="button" class="btn btn-primary btn-nobg btn-zoom--hover mb-5">
                                        <span class="btn-inner--icon"><i class="fas fa-arrow-left"></i></span>
                                    </button>
                                </a>
                                <h4 class="heading h3 text-white pt-3 pb-5">Welcome back,<br>
                                    login to your account.</h4>
                                <form class="form-primary" action="${pageContext.request.contextPath}/loginAdmin" method="post">
                                    <div class="form-group" id="outer">
                                        <input type="text" class="form-control" name="id" id="id"
                                               placeholder="ID">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password" id="password"
                                               placeholder="Password">
                                    </div>
                                    <div class="text-right mt-4"><a href="#" class="text-white">Forgot password?</a>
                                    </div>
                                    <button type="submit" class="btn btn-block btn-lg bg-white mt-4">Sign in</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<footer class="pt-5 pb-3 footer bg-tertiary">
    <div class="container" style="align-self: center ;text-align: center">
        <p>
            地址：广州市天河区中山大道西293号<br>
            邮编：510665; 传真：020-38257901<br>
            电话：020-38256601
        </p>
        <p><b>Copyright@ 2022 All Rights Reserved.</b></p>
    </div>
</footer>
<!-- Core -->
<script src="bootstrap/js/jquery.min.js"></script>
<script src="bootstrap/js/popper.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- FontAwesome 5 -->
<script src="bootstrap/js/fontawesome-all.min.js" defer></script>
<!-- Page plugins -->
<script src="bootstrap/js/bootstrap-select.min.js"></script>
<script src="bootstrap/js/bootstrap-tagsinput.min.js"></script>
<script src="bootstrap/js/input-mask.min.js"></script>
<script src="bootstrap/js/nouislider.min.js"></script>
<script src="bootstrap/js/textarea-autosize.min.js"></script>
<!-- Theme JS -->
<script src="bootstrap/js/theme.js"></script>
</body>
</html>
