<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>DownLoad--资源下载</title>
    <head>
        <meta charset="utf-8">
        <title>HOT</title>
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>

    </head>
<body style="background-color: #0f0f0f">
<nav class="navbar navbar-default" role="navigation" style="background-color: #120a1f ;border: none">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" style="color: #ffa31a"><b>VIDEOMAX•视频</b></a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}/toMain" style="color: #ffa31a;">HOME/主页</a></li>
                <li><a href="${pageContext.request.contextPath}/toHot" style="color: #ffa31a">HOT/热门</a></li>
                <li class="active"><a href="${pageContext.request.contextPath}/toSource" style="color: white;"><b>SOURCE/资源下载</b></a> </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #ffa31a">
                        CATEGORY/分类
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">内地</a></li>
                        <li><a href="#">港片</a></li>
                        <li><a href="#">日韩</a></li>
                        <li class="divider"></li>
                        <li><a href="#">用户上传</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group" align="right">
                    <input type="text" class="form-control" style="background-color: #0f0f0f" ; width="200px"
                           placeholder="Search" onkeydown="onKeyDown(event)">
                    <button type="submit" class="btn btn-default" style="background-color: #d4d450;color: white">搜索
                    </button>
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="account_center.jsp"><span class="glyphicon glyphicon-user"></span> 账户</a></li>
                <li><a href="${pageContext.request.contextPath}/toSign"><span class="glyphicon glyphicon-log-in"></span> 登录</a></li>
            </ul>
        </div>
    </div>
    <script type="text/javascript">
        function onKeyDown(event) {
            let e = event || window.event || arguments.callee.caller.arguments[0];
            if (e && e.keyCode == 27) { // 按 Esc
                //要做的事情
            }
            if (e && e.keyCode == 113) { // 按 F2
                //要做的事情
            }
            if (e && e.keyCode == 13) { // enter 键
                alert("网络连接失败！");
            }
        }
    </script>
</nav>

<table class="table table-hover" style="color: white">
    <thead><tr>
        <th>#</th>
        <th>video-name</th>
        <th>UpdateTime</th>
        <th>Download_URL</th>
    </tr></thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>《陈翔六点半》努力奋斗摆脱流浪！</td>
        <td>2022-06-05(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video1.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>2</td>
        <td>你见过这么小的奥特曼吗？迷你奥特曼</td>
        <td>2022-06-05(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video4.mp4"><span class="glyphicon glyphicon-download-alt"></span> Download</a></td>
    </tr>
    <tr>
        <td>3</td>
        <td>《王者荣耀》：谁说对抗路不能有感情！</td>
        <td>2022-06-05(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video5.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>4</td>
        <td>想我了吗？想我的话怎么不来找我！</td>
        <td>2022-06-05(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video2.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>5</td>
        <td>令人振奋！神舟十四号于今日发射！</td>
        <td>2022-06-05(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video3.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>6</td>
        <td>请记住他的名字！#D2809动车</td>
        <td>2022-06-05(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video6.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>7</td>
        <td>我的20岁生日 #广师大 #小赖不耍赖</td>
        <td>2022-06-05(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video15.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>8</td>
        <td>校园运动会VLOG #阿倩dd</td>
        <td>2022-05-30(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video11.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>9</td>
        <td>懂事的都已经艾特好兄弟来看了</td>
        <td>2022-05-30(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video13.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>10</td>
        <td>一次性看完地球46亿年的变化！</td>
        <td>2022-05-27(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video8.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>11</td>
        <td>四千年一遇的美女 #鞠婧祎</td>
        <td>2022-05-27(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video9.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>12</td>
        <td>喜欢我眼里的光吗？快艾特你的兄弟来看！</td>
        <td>2022-05-25(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video10.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>13</td>
        <td>这还不赶紧艾特你的好兄弟来看！</td>
        <td>2022-05-25(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video14.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>14</td>
        <td>辣吗？不辣我就删了</td>
        <td>2022-05-25(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video7.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>15</td>
        <td>这是哪里来的球啊？白吗？</td>
        <td>2022-05-25(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video16.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>16</td>
        <td>短发YYDS！很飒！</td>
        <td>2022-05-15(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video17.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>17</td>
        <td>《四大美女》#宝藏阁</td>
        <td>2022-05-15(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video18.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    <tr>
        <td>18</td>
        <td>《就忘了吗 DJ》 #一口甜</td>
        <td>2022-05-15(GMT)</td>
        <td> <a href="/VideoShop_war_exploded/DownloadServlet?filename=video19.mp4"><span class="glyphicon glyphicon-download-alt"></span>Download</a></td>
    </tr>
    </tbody>
</table>
</body>
</html>
