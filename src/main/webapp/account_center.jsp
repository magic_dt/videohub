<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <head>
        <meta charset="utf-8">
        <title>VIDEO•HUB--用户中心</title>
        <link rel="icon" sizes="90x90" href="imgs/logo.png">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
    </head>
<body style="background-color: #0f0f0f">
<nav class="navbar navbar-default" role="navigation" style="background-color: #120a1f ;border: none">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" style="color: #ffa31a"><b>VIDEOHUB•视频</b></a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li><a href="index.jsp" style="color: #ffa31a;">HOME/主页</a></li>
                <li><a href="hot.jsp" style="color: #ffa31a">HOT/热门</a></li>
                <li><a href="source.jsp" style="color: #ffa31a;">SOURCE/资源下载</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #ffa31a">
                        CATEGORY/分类
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">内地</a></li>
                        <li><a href="#">港片</a></li>
                        <li><a href="#">日韩</a></li>
                        <li class="divider"></li>
                        <li><a href="#">用户上传</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search" action="SearchController" method="post">
                <div class="form-group" align="right">
                    <input type="text" class="form-control" style="background-color: #0f0f0f" ; width="200px"
                           placeholder="Search" name="partname"  onkeydown="onKeyDown(event)">
                    <button type="submit" class="btn btn-default" style="background-color: #d4d450;color: white">搜索
                    </button>
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="account_center.jsp"><span class="glyphicon glyphicon-user"></span> 账户</a></li>
                <li><a href="register.jsp"><span class="glyphicon glyphicon-log-in"></span> 登录</a></li>
            </ul>
        </div>
    </div>
    <script type="text/javascript">
        function onKeyDown(event) {
            let e = event || window.event || arguments.callee.caller.arguments[0];
            if (e && e.keyCode == 27) { // 按 Esc
                //要做的事情
            }
            if (e && e.keyCode == 113) { // 按 F2
                //要做的事情
            }
            if (e && e.keyCode == 13) { // enter 键
                alert("网络连接失败！");
            }
        }
    </script>
</nav>
<div class="jumbotron" style="background: black; color: whitesmoke">
    <h1 style="position: relative;text-align: center" >WELCOME BACK MY FRIEND!</h1>
    <h2 style="color: white;position: relative;top: 20px;text-align: -webkit-right; padding-right: 4rem;">${sessionScope.usersession.username}</h2>
    <p><a class="btn btn-primary btn-lg" href="upload.jsp" role="button" style="position: relative;top: 40px;left: 620px"><span class="glyphicon glyphicon-open"></span>UPLOAD VIDEO/上传视频</a></p>
</div>

<section>
    <h1>我的收藏</h1>
</section>
<div class="container">
    <div class="row row-cols-3" id="video-area">

    </div>
</div>
<script>

</script>

</body>
</html>
