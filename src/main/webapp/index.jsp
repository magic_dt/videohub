<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<html class="element">
<head>
    <meta charset="utf-8">
    <title>VIDEO•HUB--匿名私人視頻網站</title>
    <link rel="icon" sizes="90x90" href="imgs/logo.png">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner img {
            width: 100%;
            height: 100%;
        }

        .element::-webkit-scrollbar {
             display: none; /* Chrome Safari */
         }
    </style>
</head>
<body style="background-color: #0f0f0f">
  <jsp:include page="home_header.jsp"></jsp:include>
<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="2500">
    <!-- 轮播（Carousel）指标 data-ride 自动轮播-->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <!-- 轮播（Carousel）项目 -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="imgs/img1.png" alt="First slide">
            <div class="carousel-caption">5G新时代 超潮流</div>
        </div>
        <div class="item">
            <img src="imgs/img2.png" alt="Second slide">
            <div class="carousel-caption">引领全球</div>
        </div>
        <div class="item">
            <img src="imgs/img3.png" alt="Third slide">
            <div class="carousel-caption">免费 共享</div>
        </div>
    </div>
    <!-- 轮播（Carousel）导航 -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!--页脚-->
<footer class="panel-footer" style="width: 100%; overflow: hidden; background: #0f0f0f;border: none">
    <div class="container">
        <div class="row footer-top" style="text-align: center; color: white;font-family: 微软雅黑">
            <p>
                地址：广州市天河区中山大道西293号<br>
                邮编：510665; 传真：020-38257901<br>
                电话：020-38256601
            </p>
            <p><b>Copyright© 2022 All Rights Reserved.</b></p>

        </div>
        <hr style="border: 1px  solid #000000;">
        <div class="row" style=" text-align: center;">
            <div class="col-sm-4 col-md-4 col-lg-4">
                <a href="#" style=" color: white">关于我们</a>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <a href="#" style=" color: white">联系方式</a></li>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <a href="https: //gpnu.edu.cn/" style=" color: white">特别致谢</a></li>
            </div>
        </div>
    </div>
</footer>

<script>
    //网站访问请求验证弹出
    window.onload=function (){
        const r=window.confirm("欢迎登陆本站，请确认您是否已成年？");
        if(r!=true){
            window.location.href="${pageContext.request.contextPath}/error";
        }
    }

</script>
</body>
</html>
