<%--
  Created by IntelliJ IDEA.
  User: 13241
  Date: 2022/3/1
  Time: 20:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>主页</title>
    <script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>
文件上传demo
<form enctype="multipart/form-data">
    <%--普通数据--%>
    <p><label for="user">用户：</label><input id="user" type="text" name="username"/></p>
    <%--二进制文件数据--%>
    <p><label for="file">文件：</label><input id="file" type="file" name="file"/> <br>
        <a id="uploadFile" href="javascript:void(0)" onclick="upload()">立即上传</a></p>
    <p><input id="btnsubmit" type="submit" value="提交"/></p>
</form>
<script type="text/javascript" src="./js/jquery-3.6.0.min.js"></script>
<script type="text/javascript">
    function upload(){
        //获取要上传的文件
        var file=document.getElementById("file").files[0];//二进制文件数据
        var username=document.getElementById("user").value;//普通数据
        if (file==undefined){
            alert("请先选择文件.")
            return false;
        }
        //将文件放入formData对象中
        var formData=new FormData();
        formData.append("file",file);
        formData.append("username",username);
        //ajax发送数据
        $.ajax({
            url:"${pageContext.request.contextPath}/upload/file",
            type:"POST",
            data:formData,
            processData:false,
            contentType:false,
            success:function (result) {
                console.log(result)
            }
        })
    }

</script>
</body>
</html>

