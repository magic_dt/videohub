<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>VIDEO•HUB--管理后台</title>
    <meta charset="UTF-8">
    <link rel="icon" sizes="90x90" href="imgs/logo.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--Bootstrap与JQuery-->
    <link
            href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.css"
            rel="stylesheet">
    <link
            href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.min.css"
            rel="stylesheet">
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script>
    <link
            href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap-utilities.rtl.css"
            rel="stylesheet">
    <!--bootstrap js-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>

<%--    <link href="css/style.css" rel="stylesheet">--%>
    <style>
        .data{
            position: relative;
            top: 80px;
            text-align: center;
        }
        button.btn.btn-primary {
            margin-right: 0.5rem;
        }
        a{
            text-decoration: none;
        }
        .dark{
            background-color: #17181a;
            color: white;
        }
    </style>
</head>
<body>
<!--导航栏-->
<nav class="navbar navbar-light bg-light fixed-top" id="nav">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"><strong>VIDEO•HUB ${sessionScope.adminsession.name}</strong></a>
        <button class="navbar-toggler" type="button"
                data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar"
                aria-controls="offcanvasNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="offcanvas offcanvas-end" tabindex="-1"
             id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasNavbarLabel">导航栏</h5>
                <button type="button" class="btn-close"
                        data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <ul class="navbar-nav justify-content-end flex-grow-1
                            pe-3">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page"
                           href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="mode" href="#" onclick="changeMode()">DARK</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#"
                           id="offcanvasNavbarDropdown" role="button"
                           data-bs-toggle="dropdown"
                           aria-expanded="false">
                            Dropdown
                        </a>
                        <ul class="dropdown-menu"
                            aria-labelledby="offcanvasNavbarDropdown">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another
                                action</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="#">Something
                                else here</a></li>
                        </ul>
                    </li>
                </ul>
                <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search"
                           placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success"
                            type="submit">Search</button>
                </form>
            </div>
        </div>
    </div>
</nav>

<main>
    <div class="data table-responsive">
        <h1 style="margin-bottom: 1rem">举报处理</h1>
        <table class="table table-hover align-middle" id="table">
            <thead>
            <tr>
                <th scope="col">序号</th>
                <th scope="col">用户ID</th>
                <th scope="col">举报视频</th>
                <th scope="col">举报原因</th>
                <th scope="col">操作</th>
            </tr>
            </thead>
            <tbody id="report-table">

            </tbody>
        </table>
    </div>
</main>

<script>
    $(window).ready(function (){
        $.get("${pageContext.request.contextPath}/getReport",
            {},
            function (res){
            let i=0;
            for(i=0;i<res.length;i++) {
                $("#report-table").append("<tr>" +
                    "<th scope='row'>" + i + "</th>" +
                    "<td>" + res[i].user_id + "</td>" +
                    "<td><a href='javascript:void(0);' onclick='toPlay("+res[i].video_id+")'>预览</a></td>" +
                    "<td>" + res[i].reason + "</td>" +
                    "<td colspan='4'>" +
                    "<button class='btn btn-primary' id='remain' onclick='Stay("+res[i].user_id+","+res[i].video_id+","+i+")'>保留</button>" +
                    "<button class='btn btn-danger' id='delete' onclick='Move("+res[i].user_id+","+res[i].video_id+","+i+")'>删除</button>" +
                    "</td>" +
                    "</tr>")
            }})}
    )

    //删除举报记录 表示视频合规
    function Stay(m,n,i){
        $.post("${pageContext.request.contextPath}/delReport",
            {
                user_id:m,
                video_id:n
            },
            function (res){
                console.log(res)
            }
        );
        $('table tr').eq(i+1).remove();
        // //延时刷新页面
        // setTimeout(function (){
        //     window.location.reload();
        // },2000);
        alert("操作成功！")
    }

    //删除相关视频和举报记录
    function Move(m,n,i){
        $.post("${pageContext.request.contextPath}/delReport",
            {
                user_id:m,
                video_id:n
            },
            function (){
                console.log("success")
            }
        )
        //删除视频
        <%--$.get("${pageContext.request.contextPath}/deleteVideo",--%>
        <%--    {--%>
        <%--        video_id:n--%>
        <%--    },function (res){--%>
        <%--        console.log(res);--%>
        <%--    }--%>
        <%--)--%>
        $('table tr').eq(i+1).remove();
        alert("操作成功！")
    }

    //记录在本地会话 跨页面传参
    function toPlay(n){
        window.localStorage.setItem('video_id', n);
        //新开一个标签页播放
        window.open("${pageContext.request.contextPath}/watchVideo");
        //直接跳转
        <%--window.location.href="${pageContext.request.contextPath}/watchVideo";--%>
    }

    var Mode=0;
    //日夜间模式
    function changeMode(){
        $("body").toggleClass("dark");
        $("#nav").toggleClass("navbar-light bg-light");
        $("#nav").toggleClass("navbar-dark bg-dark");
        $("#table").toggleClass("table-dark");
        $("#offcanvasNavbar").toggleClass("dark");
        if(Mode==0){
            $("#mode").html("LIGHT");
            Mode=1;
        }else{
            $("#mode").html("DARK");
            Mode=0;
        }
    }

</script>
</body>
</html>
