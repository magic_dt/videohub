<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html class="element">
<head>
    <meta charset="utf-8">
    <title>PORN HUB</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <style>
        .element::-webkit-scrollbar {
            display: none; /* Chrome Safari */
        }
    </style>
</head>
<body>
<nav class="navbar navbar-default" role="navigation" style="background-color: #120a1f ;border: none">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" style="color: #ffa31a"><b>VIDEOMAX•视频</b></a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#" style="color: white;"><b>HOME/主页</b></a></li>
                <li><a href="${pageContext.request.contextPath}/toHot" style="color: #ffa31a">HOT/热门</a></li>
                <li><a href="${pageContext.request.contextPath}/toSource" style="color: #ffa31a;">SOURCE/资源下载</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #ffa31a">
                        CATEGORY/分类
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">内地</a></li>
                        <li><a href="#">港片</a></li>
                        <li><a href="#">日韩</a></li>
                        <li class="divider"></li>
                        <li><a href="#">用户上传</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search" action="SearchController">
                <div class="form-group" align="right">
                    <input type="text" class="form-control" style="background-color: #0f0f0f" ; width="200px"
                           placeholder="Search" name="partName" onkeydown="onKeyDown(event)">
                    <button type="submit" class="btn btn-default" style="background-color: #d4d450;color: white">搜索
                    </button>
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span> 账户</a></li>
                <li><a href="${pageContext.request.contextPath}/toSign"><span class="glyphicon glyphicon-log-in"></span> 登录</a></li>
            </ul>
        </div>
    </div>
</nav>
    <script type="text/javascript">
        function onKeyDown(event) {
            let e = event || window.event || arguments.callee.caller.arguments[0];
            if (e && e.keyCode == 27) { // 按 Esc
                //要做的事情
            }
            if (e && e.keyCode == 113) { // 按 F2
                //要做的事情
            }
            if (e && e.keyCode == 13) { // enter 键
                alert("网络连接失败！");
            }
        }
    </script>
</body>
</html>
