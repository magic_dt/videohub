<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!-- 技术栈 Bootstrap JQuery SSM -->
<html lang="en" class="element">
<head>
    <title>VIDEO•HUB--匿名私人視頻網站</title>
    <meta charset="UTF-8">
    <link rel="icon" sizes="90x90" href="imgs/logo.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Bootstrap & JQuery-->
    <link
            href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.css"
            rel="stylesheet">
    <link
            href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.min.css"
            rel="stylesheet">
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script>
    <link
            href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap-utilities.rtl.css"
            rel="stylesheet">

    <!--css-->
    <link href="css/newhot.css" rel="stylesheet">
    <!--fonticon 库-->
    <script src="js/iconfont.js"></script>
    <style>
        .icon {
            width: 4em;
            height: 4em;
            /* vertical-align: -0.15em; */
            /* justify-content: center; */
            fill: currentColor;
            overflow: hidden;
        }
    </style>
</head>

<!--更改夜间模式需在body加dark，以及导航栏，video-name，-->
<body class="dark">

<!--BootStrap导航栏-->
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" id="nav">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="imgs/logo.png" alt="" width="32" height="28"
                 class="d-inline-block align-text-top">
            <strong>IDEO•HUB</strong>
        </a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-4 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="${pageContext.request.contextPath}/toMain">HOME</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="${pageContext.request.contextPath}/toNewHot">HOT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" id="mode" onclick="changeMode();" title="日夜间模式">LIGHT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/toAccount">ACCOUNT</a>
                </li>
            </ul>
            <div class="d-flex" role="search" action="#">
                <!-- <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search"> -->
                <input class="search" type="search" name="key" id="key" placeholder="搜索...">
                <button class="btn btn-outline-success" id="search" onclick="search()">Search</button>
            </div>
        </div>
    </div>
</nav>

<!--视频内容-->
<main>
    <div class="container">
        <div class="row row-cols-3" id="video-area">

        </div>
    </div>
</main>

<!--回到顶部-->
<div class="totop dark" id="totop">
    <svg t="1670228643173" class="icon" viewBox="0 0 1024 1024"
         version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3885"
         width="200" height="200"><path d="M512 843.2c-36.2
              0-66.4-13.6-85.8-21.8-10.8-4.6-22.6 3.6-21.8 15.2l7 102c0.4 6.2
              7.6 9.4 12.6 5.6l29-22c3.6-2.8 9-1.8 11.4 2l41 64.2c3 4.8 10.2 4.8
              13.2 0l41-64.2c2.4-3.8 7.8-4.8 11.4-2l29 22c5 3.8 12.2 0.6
              12.6-5.6l7-102c0.8-11.6-11-20-21.8-15.2-19.6 8.2-49.6 21.8-85.8
              21.8z" p-id="3886" fill="#2025c7"></path><path d="M795.4
              586.2l-96-98.2c0-316-186.4-456-186.4-456s-188.2 140-188.2 456l-96
              98.2c-3.6 3.6-5.2 9-4.4 14.2L261.2 824c1.8 11.4 14.2 17 23.6
              10.8l134.2-90.8s41.4 40 94.2 40c52.8 0 92.2-40 92.2-40l134.2
              90.8c9.2 6.2 21.6 0.6
              23.6-10.8l37-223.8c0.4-5.2-1.2-10.4-4.8-14zM513 384c-34
              0-61.4-28.6-61.4-64s27.6-64 61.4-64c34 0 61.4 28.6 61.4 64s-27.4
              64-61.4 64z" p-id="3887" fill="#2025c7"></path></svg>
</div>

<script>
    //全局变量，监测日夜间模式
    var Mode=1;

    //视频鼠标监测播放
    //鼠标移进去
    function videoPlayback(n){
        //获取视频标签
        let video = document.getElementsByName("video");
        //给视频标签添加缓存播放---video标签属性
        video[n].setAttribute("autoplay","autoplay");
        //给视频标签添加循环播放---video标签属性
        video[n].setAttribute("loop","loop");
        //添加视频控件
        video[n].setAttribute("controls","true");
        //禁用原始下载功能
        video[n].setAttribute("controlslist","nodownload noremoteplayback nofullscreen");
        //播放视频
        video[n].play();
    }

    //鼠标离开
    function videoStopped(n){
        //获取视频标签
        let video = document.getElementsByName("video");
        //移除控件
        video[n].removeAttribute("controls");
        //停止播放
        video[n].pause();
    }


    //悬停时间超4秒全屏播放



    //日夜间模式
    function changeMode(){
        $("body").toggleClass("dark");
        $("#nav").toggleClass("navbar-light bg-light");
        $("#nav").toggleClass("navbar-dark bg-dark");
        $("#totop").toggleClass("dark");
        $("a.link").toggleClass("dark");
        if(Mode==0){
            $("#mode").html("LIGHT");
            Mode=1;
        }else{
            $("#mode").html("DARK");
            Mode=0;
        }
    }


    //回到顶部
    let totop = document.querySelector('.totop')
    // 页面滚动窗口监听事件
    window.onscroll = function() {
        // 获取浏览器卷去的高度
        let high = document.documentElement.scrollTop || document.body.scrollTop
        if (high >= 150) {
            totop.style.display = 'block'
        } else {
            totop.style.display = 'none'
        }
    }
    // 点击返回顶部，可以加动画使其慢慢回到顶部
    totop.addEventListener('click',() => {
        document.documentElement.scrollTop = 0
        document.body.scrollTop = 0
    })

    //记录在本地会话 跨页面传参
    function toPlay(n){
        //n为video_id
        window.localStorage.setItem('video_id', n);
        //新开一个标签页播放
        window.open("${pageContext.request.contextPath}/watchVideo");
        //直接跳转
        <%--window.location.href="${pageContext.request.contextPath}/watchVideo";--%>
    }


    //ajax请求实现页面渲染
    $(window).ready(function (){
        $.ajax({
            url:"${pageContext.request.contextPath}/allVideo",
            type:"post",
            contentType:"application/json;charset=UTF-8",
            dataType:"json",
            success:function (result){
                //计数器
                let count=0;
                //多次遍历 增加数目
                for(let j=0;j<5;j++){
                    for(let i=0; i<result.length; i++){
                        //浏览量高于21000，标红处理
                        if(result[i].time>21000){
                            var t="class='link hot dark'";
                        }else {
                            var t="class='link dark'";
                        }
                        $("#video-area").append("<div class='col'>"+
                            "<div class='video-card'>\n"+
                            "<video name='video' id='video' onmouseover='videoPlayback("+count+")' onmouseout='videoStopped("+count+")'>"+
                            "<source src="+result[i].url+" type='video/mp4'>"+
                            "</video>"+
                            "<div class='video-name'>"+
                            "<a href='javascript:void(0);' "+t+"onclick='toPlay("+result[i].video_id+")'></b>"+
                            "<svg t='1670324538380' viewBox='0 0 1024 1024' \n"+
                            "version='1.1' xmlns='http://www.w3.org/2000/svg' p-id='3533'\n"+
                            " width='20px' height='20px'><path d='M964.720538 170.38988a33.214402\n"+
                            " 33.214402 0 0 0-9.964321 0 369.344145 369.344145 0 0 1-103.961077\n"+
                            " 14.946481A370.008433 370.008433 0 0 1 538.247622 14.946481a33.214402\n"+
                            " 33.214402 0 0 0-54.803763 0 370.340577 370.340577 0 0 1-312.879663\n"+
                            " 171.718456 370.672721 370.672721 0 0 1-101.968212-14.282193 33.214402\n"+
                            " 33.214402 0 0 0-9.632177 0 33.214402 33.214402 0 0 0-33.214401\n"+
                            " 33.214402v332.144015c0 265.715212 365.358417 486.258839 486.258838\n"+
                            " 486.258839 94.3289 0 486.258839-218.882906 486.258839-486.258839V203.272138a33.214402\n"+
                            " 33.214402 0 0 0-33.546545-32.882258z m-186.664937 259.072332l-265.715213\n"+
                            " 227.850795a33.214402 33.214402 0 0 1-43.178722 0 30.225105 30.225105 0\n"+
                            " 0 1-4.317872-2.657152l-143.818359-136.179047a33.214402 33.214402 0 0\n"+
                            " 1 45.503731-48.160882L490.418883 587.562764l242.797276-208.254298a33.214402\n"+
                            " 33.214402 0 1 1 43.510866 50.153746z' fill='#22a6ef' p-id='3534'></path></svg><b>"
                            +" "+result[i].name+"</a>"+
                            "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16'"+
                            "fill='currentColor' class='bi bi-badge-hd-fill' viewBox='0 0\n"+
                            "16 16'>\n"+
                            "<path d='M10.53 5.968h-.843v4.06h.843c1.117 0 1.622-.667\n"+
                            "1.622-2.02 0-1.354-.51-2.04-1.622-2.04z'/>\n"+
                            "<path d='M2 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h12a2 2 0 0 0\n"+
                            "2-2V4a2 2 0 0 0-2-2H2zm5.396\n"+
                            "3.001V11H6.209V8.43H3.687V11H2.5V5.001h1.187v2.44h2.522V5h1.187zM8.5\n"+
                            "11V5.001h2.188c1.824 0 2.685 1.09 2.685 2.984C13.373 9.893\n"+
                            "12.5 11 10.69 11H8.5z'/>\n"+
                            "</svg>"+
                            "<div class='video-info'>"+"浏览量："+result[i].time+"</div>"+
                            "</div>");
                        count++;
                    }
                }
            }
        });
    })

    //search JS提供模糊搜索，由于视频库数量少，采用基于视频名及标签搜索
    //由于初始页面为夜间模式，所以在日间模式搜索，渲染后可能出现页面部件与网页色基调不符
    //因此使用全局变量Mode来判断需要渲染的类型
    function search(){
        //获取搜索关键词
        const key=$("#key").val();
        $.ajax({
            url:"${pageContext.request.contextPath}/search",
                type:"get",
                contentType:"application/json;charset=UTF-8",
                data:"key="+key,
                dataType:"json",
                success:function (result){
                    //清除渲染内容
                    $("#video-area").empty();

                    //根据网页模式切换渲染的class
                    if(Mode==1){
                        var m="class='link dark'";
                    }else {
                        var m="class='link'";
                    }

                    //搜索为空返回提示
                    if(result.length==0){
                        $("#video-area").append("<b>搜索无结果，可以尝试其他关键词</b>")
                    }
                    //遍历渲染
                    for(let i=0; i<result.length; i++){
                        $("#video-area").append("<div class='col'>"+
                            "<div class='video-card'>\n"+
                            "<video name='video' id='video' onmouseover='videoPlayback("+i+")' onmouseout='videoStopped("+i+")'>"+
                            "<source src="+result[i].url+" type='video/mp4'>"+
                            "</video>"+
                            "<div class='video-name'>"+
                            "<a href='javascript:void(0);' "+m+"onclick='toPlay("+result[i].video_id+")'><b>"+result[i].name+"</b></a>"+
                            "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16'"+
                            "fill='currentColor' class='bi bi-badge-hd-fill' viewBox='0 0\n"+
                            "16 16'>\n"+
                            "<path d='M10.53 5.968h-.843v4.06h.843c1.117 0 1.622-.667\n"+
                            "1.622-2.02 0-1.354-.51-2.04-1.622-2.04z'/>\n"+
                            "<path d='M2 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h12a2 2 0 0 0\n"+
                            "2-2V4a2 2 0 0 0-2-2H2zm5.396\n"+
                            "3.001V11H6.209V8.43H3.687V11H2.5V5.001h1.187v2.44h2.522V5h1.187zM8.5\n"+
                            "11V5.001h2.188c1.824 0 2.685 1.09 2.685 2.984C13.373 9.893\n"+
                            "12.5 11 10.69 11H8.5z'/>\n"+
                            "</svg>"+
                            "<div class='video-info'>"+"浏览量："+result[i].time+"</div>"+
                            "</div>");
                    }
                }
        })
    }

    //ajax请求实现页面渲染 最初
    <%--$(window).ready(function (){--%>
    <%--    $.ajax({--%>
    <%--        url:"${pageContext.request.contextPath}/allVideo",--%>
    <%--        type:"post",--%>
    <%--        contentType:"application/json;charset=UTF-8",--%>
    <%--        dataType:"json",--%>
    <%--        success:function (result){--%>
    <%--            for(let i=0; i<result.length; i++){--%>
    <%--                $("#video-area").append("<div class='col'>"+--%>
    <%--                    "<div class='video-card'>\n"+--%>
    <%--                    "<video name='video' id='video' onmouseover='videoPlayback("+i+")' onmouseout='videoStopped("+i+")'>"+--%>
    <%--                    "<source src="+result[i].url+" type='video/mp4'>"+--%>
    <%--                    "</video>"+--%>
    <%--                    "<div class='video-name'>"+--%>
    <%--                    "<a href=\"#\" class='link dark'><b>"+result[i].name+"</b></a>"+--%>
    <%--                    "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16'"+--%>
    <%--                    "fill='currentColor' class='bi bi-badge-hd-fill' viewBox='0 0\n"+--%>
    <%--                    "16 16'>\n"+--%>
    <%--                    "<path d='M10.53 5.968h-.843v4.06h.843c1.117 0 1.622-.667\n"+--%>
    <%--                    "1.622-2.02 0-1.354-.51-2.04-1.622-2.04z'/>\n"+--%>
    <%--                    "<path d='M2 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h12a2 2 0 0 0\n"+--%>
    <%--                    "2-2V4a2 2 0 0 0-2-2H2zm5.396\n"+--%>
    <%--                    "3.001V11H6.209V8.43H3.687V11H2.5V5.001h1.187v2.44h2.522V5h1.187zM8.5\n"+--%>
    <%--                    "11V5.001h2.188c1.824 0 2.685 1.09 2.685 2.984C13.373 9.893\n"+--%>
    <%--                    "12.5 11 10.69 11H8.5z'/>\n"+--%>
    <%--                    "</svg>"+--%>
    <%--                    "<div class='video-info'>"+"浏览量："+result[i].time+"</div>"+--%>
    <%--                    "</div>");--%>
    <%--            }--%>

    <%--        }--%>
    <%--    });--%>
    <%--})--%>

</script>
</body>
</html>
