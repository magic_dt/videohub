<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>

<html class="element">
<head>
    <meta charset="utf-8">
    <title>HOT</title>
    <link rel="icon" sizes="90x90" href="imgs/logo.png">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <style>
        .video-card {
            position: relative;
            font-size: 15px;
            width: 400px;
            height: 80px;
            border: none;;
            background:black;
        }
        .video-padding{
            width: 400px;
            height: 50px;
        }
        .element::-webkit-scrollbar {
            display: none; /* Chrome Safari */
        }
    </style>

</head>
<body style="background-color: #0f0f0f">
<nav class="navbar navbar-default" role="navigation" style="background-color: #120a1f ;border: none">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" style="color: #ffa31a"><b>VIDEOMAX•视频</b></a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li><a href="index.jsp" style="color: #ffa31a;">HOME/主页</a></li>
                <li class="active"><a href="hot.jsp" style="color: white"><b>HOT/热门</b></a></li>
                <li><a href="source.jsp" style="color: #ffa31a;">SOURCE/资源下载</a> </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #ffa31a">
                        CATEGORY/分类
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">内地</a></li>
                        <li><a href="#">港片</a></li>
                        <li><a href="#">日韩</a></li>
                        <li class="divider"></li>
                        <li><a href="#">用户上传</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search" action="SearchController" >
                <div class="form-group" align="right">
                    <input type="text" class="form-control" style="background-color: #0f0f0f" ; width="200px"
                           placeholder="Search" name="partname" onkeydown="onKeyDown(event)">
                    <button type="submit" class="btn btn-default" style="background-color: #d4d450;color: white">搜索
                    </button>
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="account_center.jsp"><span class="glyphicon glyphicon-user"></span> 账户</a></li>
                <li><a href="login.jsp"><span class="glyphicon glyphicon-log-in"></span> 登录</a></li>
            </ul>
        </div>
    </div>
    <script type="text/javascript">
        function onKeyDown(event) {
            let e = event || window.event || arguments.callee.caller.arguments[0];
            if (e && e.keyCode == 27) { // 按 Esc
                //要做的事情
            }
            if (e && e.keyCode == 113) { // 按 F2
                //要做的事情
            }
            if (e && e.keyCode == 13) { // enter 键
                alert("网络连接失败！");
            }
        }
    </script>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 ">
            <div class="video">
                <video width="400px" height="400px" name="video" onmouseover="videoPlayback(0)" onmouseout="videoStopped(0)" controls >
                    <source src="source/video1.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">《陈翔六点半》努力奋斗摆脱流浪！</div>
            <div class="video-padding"></div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <div class="video">
                <video width="400px" height="400px" name="video" onmouseover="videoPlayback(1)" onmouseout="videoStopped(1)"  controls>
                    <source src="source/video4.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">你见过这么小的奥特曼吗？迷你奥特曼</div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <div class="video">
                <video width="400px" height="400px" name="video" onmouseover="videoPlayback(2)" onmouseout="videoStopped(2)" controls>
                    <source src="source/video5.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">《王者荣耀》：谁说对抗路不能有感情！</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 ">
            <div class="video">
                <video width="400px" height="600px" name="video" onmouseover="videoPlayback(3)" onmouseout="videoStopped(3)" controls>
                    <source src="source/video2.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">想我了吗？想我的话怎么不来找我！</div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <div class="video">
                <video width="400px" height="600px" name="video" onmouseover="videoPlayback(4)" onmouseout="videoStopped(4)" controls>
                    <source src="source/video3.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">令人振奋！神舟十四号于今日发射！</div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <div class="video">
                <video width="400px" height="600px" name="video" onmouseover="videoPlayback(5)" onmouseout="videoStopped(5)" controls>
                    <source src="source/video6.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">请记住他的名字！#D2809动车</div>
            <div class="video-padding"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 ">
            <div class="video">
                <video width="400px" height="600px" name="video" onmouseover="videoPlayback(6)" onmouseout="videoStopped(6)" controls>
                    <source src="source/video15.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">我的20岁生日 #广师大 #小赖不耍赖</div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <div class="video">
                <video width="400px" height="600px" name="video" onmouseover="videoPlayback(7)" onmouseout="videoStopped(7)" controls>
                    <source src="source/video11.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">校园运动会VLOG #阿倩dd</div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <div class="video">
                <video width="400px" height="600px" name="video" onmouseover="videoPlayback(8)" onmouseout="videoStopped(8)" controls>
                    <source src="source/video13.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">懂事的都已经艾特好兄弟来看了</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 ">
            <div class="video">
                <video width="400px" height="400px" name="video" onmouseover="videoPlayback(9)" onmouseout="videoStopped(9)" controls>
                    <source src="source/video8.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">一次性看完地球46亿年的变化！</div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <div class="video">
                <video width="400px" height="400px" name="video" onmouseover="videoPlayback(10)" onmouseout="videoStopped(10)" controls>
                    <source src="source/video9.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">四千年一遇的美女 #鞠婧祎</div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <div class="video">
                <video width="400px" height="400px" name="video" onmouseover="videoPlayback(11)" onmouseout="videoStopped(11)" controls>
                    <source src="source/video10.mp4" type="video/mp4">
                </video>
            </div>
            <div class="video-card" style="text-align: center;color: white;">喜欢我眼里的光吗？快艾特你的兄弟来看！</div>
            <div class="video-padding"></div>
        </div>
    </div>


</div>






<script>
    //鼠标移进去
    function videoPlayback(n){
        //获取视频标签
        let video = document.getElementsByName("video");
        //给视频标签添加缓存播放---video标签属性
        video[n].setAttribute("autoplay","autoplay")
        //给视频标签添加循环播放---video标签属性
        video[n].setAttribute("loop","loop")
        //播放视频
        video[n].play();
    }

    //鼠标离开
    function videoStopped(n){
        //获取视频标签
        let video = document.getElementsByName("video");
        //停止播放
        video[n].pause();
    }
</script>

<!--页脚-->
<footer class="panel-footer" style="width: 100%; overflow: hidden; background: #0f0f0f;border: none">
    <div class="container">
        <div class="row footer-top" style="text-align: center; color: white;font-family: 微软雅黑">
            <p>
                地址：广州市天河区中山大道西293号<br>
                邮编：510665; 传真：020-38257901<br>
                电话：020-38256601
            </p>
            <p><b>Copyright© 2022 All Rights Reserved.</b></p>

        </div>
        <hr style="border: 1px  solid #000000;">
        <div class="row" style="text-align: center;">
            <div class="col-sm-4 col-md-4 col-lg-4">
                <a href="#" style=" color: white">关于我们</a>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <a href="#" style=" color: white">联系方式</a></li>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <a href="https://gpnu.edu.cn/" style=" color: white">特别致谢</a></li>
            </div>
        </div>
    </div>
</footer>

<script>
    //弹出框提示跳转新版网站
    window.onload=function (){
        const r=window.confirm("GOOD NEWS: 本站已有新版网站，欢迎体验新版！（单击确认跳转）");
        if(r==true){
            window.location.href="${pageContext.request.contextPath}/toNewHot";
        }
    }
</script>
</body>
</html>
