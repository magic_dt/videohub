<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>404-出错了</title>
    <meta charset="UTF-8">
    <style>
        html {
            margin: 0;
            padding: 0;
            background-color: white;
        }

        body,
        html {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        #svgContainer {
            width: 640px;
            height: 512px;
            background-color: white;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
        }
    </style>
</head>

<body>
<div id="svgContainer"></div>
<script src='js/eEjVBX.js'></script>
<script src='js/MvBBBj.js'></script>
<script src="js/script.js"></script>
</body>

</html>

