package com.videohub.mapper;

import com.videohub.domain.Video;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/*.xml"})
public class VideoMapperTest {
    @Autowired
    public VideoMapper videoMapper;

    @Test
    public void selectAll() {
        List<Video> videos = videoMapper.selectAll();
        System.out.println(Arrays.asList(videos));
    }

}
